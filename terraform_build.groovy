job('terraform_aws') {
    logRotator(-1,5,-1-1)
    triggers { scm("H/2 * * * *") }
    scm {
        git {
            remote {
                url("git@bitbucket.org:frecra/terraform.git")
                credentials('859b52c0-760f-418a-b1a8-9479e0e08eca')
            }

            extensions {
                pathRestriction {
                    includedRegions(null)
                    excludedRegions('aws/helloworld/.*\\.tfstate.*')
                }
                localBranch('master')
            }
            branch('master')

        }
    }
    steps {
        shell('cd aws/helloworld\n' +
                '$TERRAFORM_BIN apply -no-color -var-file=production.tfvars\n' +
                'status=$?\n' +
                'git commit -a -m "Jenkins Build 1.0.${BUILD_NUMBER}"\n' +
                'git push origin master\n' +
                'exit $status'
        )
    }
    publishers {
        git {
            pushOnlyIfSuccess()
            pushMerge()
            tag('origin', 'Build_1.0.${BUILD_NUMBER}') {
                message('Release 1.0.{$BUILD_NUMBER}')
                create()
            }
        }
    }
}
