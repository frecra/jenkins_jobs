job('website_deploy') {
    logRotator(-1, 5, -1 - 1)
    triggers { scm("H/2 * * * *") }
    wrappers {
        nodejs('NodeJS 6.10')
    }
    scm {
        git {
            remote {
                url("git@bitbucket.org:frecra/website.git")
                credentials('859b52c0-760f-418a-b1a8-9479e0e08eca')
            }
            extensions {
                localBranch('master')
            }
            branch('refs/heads/master')
        }
    }
    steps {
        shell('npm install')
        shell('npm test')
        shell('rm -rf node_modules')
    }
    publishers {
        s3BucketPublisher {
            profileName('jenkins')
            entries {
                entry {
                    bucket('frecra.com')
                    sourceFile('*')
                    storageClass('STANDARD')
                    selectedRegion('eu-west-1')
                    noUploadOnFailure(true)
                    excludedFile('')
                    uploadFromSlave(false)
                    managedArtifacts(false)
                    useServerSideEncryption(false)
                    flatten(false)
                    gzipFiles(false)
                    keepForever(false)
                    showDirectlyInBrowser(false)
                }
            }
            dontWaitForConcurrentBuildCompletion(false)
            consoleLogLevel('INFO')
            pluginFailureResultConstraint('FAILURE')
        }
    }
}