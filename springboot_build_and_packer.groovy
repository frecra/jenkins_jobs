mavenJob('frecra_springboot_java_build_and_bake') {
    triggers { scm("H/2 * * * *") }
    scm {
        git {
            remote {
                url("https://askehill@bitbucket.org/frecra/helloworld.git")
            }
            branch('master')
        }
    }
    goals('clean install')
    preBuildSteps {
        shell('mvn versions:set -DnewVersion=1.0.$BUILD_NUMBER')
    }
    postBuildSteps {
        shell('mvn versions:revert')
    }
    publishers {
        s3BucketPublisher {
            profileName('jenkins')
            entries {
                entry {
                    bucket('demos.frecra.com')
                    sourceFile('target/*.jar')
                    storageClass('STANDARD')
                    selectedRegion('eu-west-1')
                    noUploadOnFailure(true)
                    excludedFile('')
                    uploadFromSlave(false)
                    managedArtifacts(false)
                    useServerSideEncryption(false)
                    flatten(false)
                    gzipFiles(false)
                    keepForever(false)
                    showDirectlyInBrowser(false)
                }
            }
            dontWaitForConcurrentBuildCompletion(false)
            consoleLogLevel('INFO')
            pluginFailureResultConstraint('FAILURE')
        }
        packerPublisher {
            name('0.12.2')
            jsonTemplate("packer.json")
            jsonTemplateText(null)
            packerHome('')
            params('-var app_version_number=1.0.${BUILD_NUMBER}')
            useDebug(false)
            changeDir('')
        }
    }
}